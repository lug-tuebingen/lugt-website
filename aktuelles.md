---
layout: default
title: Aktuelles
permalink: /Aktuelles/
---

## Aktuelles

{% for post in site.posts %}
  * {{ post.date | date: "%b %-d, %Y" }} [{{ post.title }}]({{ post.url | prepend: site.baseurl }})
{% endfor %}
