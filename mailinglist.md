---
layout: default
title: Mailinglist
permalink: /Mailinglist/
---

## Mailingliste

Die Uni Tübingen betreibt eine Mailingliste zum Thema GNU+Linux Ökosystem. Ihr darf jeder unter folgendem Link beitreten: [listserv.uni-tuebingen.de/mailman/listinfo/lug-tuebingen](https://listserv.uni-tuebingen.de/mailman/listinfo/lug-tuebingen)
