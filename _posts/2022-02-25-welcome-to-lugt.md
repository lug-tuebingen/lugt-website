---
layout: post
title:  "Wilkommen in der LUGT"
date:   2022-02-25 18:00:00 CET
categories: linux
---
Diese Seite findest du im `_posts` Verzeichnis.

Jekyll hat außerdem gutes Code Highlighting für Code Snippets.

{% highlight python %}
#!/usr/bin/python3
print("Hello, world!")
#=> prints 'Hello, World!' to STDOUT.
{% endhighlight %}
