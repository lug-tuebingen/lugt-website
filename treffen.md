---
layout: default
title: Treffen
permalink: /Treffen/
---

## Treffen

Wir treffen uns jeden dritten Dienstag im Monat zum Stammtisch.

{% assign today = 'now' | date: "%Y-%m-%d" %}
{% assign descending = site.data.meetings | sort:"date" | reverse %}

### nächster Stammtisch:

{% for meeting in site.data.meetings %}
  {% assign next_meeting = meeting.date | date: "%Y-%m-%d" %}
  {% if next_meeting >= today %}
  * {{ meeting.date | date: "%Y-%m-%d,%t%H Uhr" }}: {{ meeting.location }}
    {% break %}
  {% endif %}
{% endfor %}

### zukünftige Termine:

{% for dmeeting in descending %}
  {% assign future_meeting = dmeeting.date | date: "%Y-%m-%d" %}
  {% if future_meeting == next_meeting %}
    {% continue %}
  {% endif %}
  {% if future_meeting > today %}
  * {{ dmeeting.date | date: "%Y-%m-%d,%t%H Uhr" }}: {{ dmeeting.location }}
  {% endif %}
{% endfor %}

### vergangene Termine:

{% for dmeeting in descending %}
  {% assign past_meeting = dmeeting.date | date: "%Y-%m-%d" %}
  {% if past_meeting == next_meeting %}
    {% continue %}
  {% endif %}
  {% if past_meeting < today %}
  * {{ dmeeting.date | date: "%Y-%m-%d,%t%H Uhr" }}: {{ dmeeting.location }}
  {% endif %}
{% endfor %}
